# exprsn

(Pronounced: Expression)

exprsn is a mini expression language useful for evaluating conditions in cases like in
config files which describe a UI which must be reactive.  No claims are made
about the performance (though it should be quick enough to be useful). The main
goals is ease of use and small size.

## Keywords and Symbols

The full list of supported keywords and symbols are provided below

### Symbols

Symbol     | Description
-----------|------------
and        | Conjunction which checks that both left and right arguments are true.
or         | Conjunction which checks that either left or right arguments are true.
\>         | "greater than" comparison check. Evaluates to true if the left argument is greater than the right argument.
<          | "less than" comparison check. Evaluates to true if the left argument is less than the right argument.
==         | Equality check. Check that the left argument is equal to the right argument.
!=         | Equality check. Check that the left argument is not equal to the right argument.
(          | Open parenthesis, for starting a parenthized group.
)          | Close parenthesis, for ending a parenthized group.
true       | Boolean true value.
false      | Boolean false value.

### Identifiers

Identifiers can be an ascii sequence in the set
`0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_`, but which is
not one of the existing key words: `and`, `or`, `true`, `false`.

### Types

Type            | Description
----------------|------------
Float (32bit)   |
String          |
Int (32bit)     |
Bool            |