#!/usr/bin/env python3
import sys
import re
import json

start   = re.compile("^running (\d+) tests$")
test    = re.compile("^test (.+) ... (.+)$")
summary = re.compile("^test result: (.+?). (\d+) passed; (\d+) failed; (\d+) ignored; (\d+) measured; (\d+) filtered out; finished in (.+)$")

def start_dict(count):
    return { "type": "suite", "event": "started", "test_count": int(count) }

def test_dict(name, event):
    return { "type": "test", "name": name, "event": event }

def summary_dict(result, passed, failed, ignored, measured, filtered_out, exec_time):
    exec_time = float(exec_time.rstrip("s"))
    return { "type": "suite", "event": result, "passed": int(passed), "failed": int(failed), "ignored": int(ignored), "measured": int(measured), "filtered_out": int(filtered_out), "exec_time": exec_time }

for line in sys.stdin.readlines():
    start_res = start.match(line)
    test_res = test.match(line)
    summary_res = summary.match(line)

    if start_res:
        print(json.dumps(start_dict(start_res.group(1))))
    elif summary_res:
        print(json.dumps(summary_dict(*summary_res.groups())))
    elif test_res:
        print(json.dumps(test_dict(test_res.group(1), "started")))
        print(json.dumps(test_dict(test_res.group(1), test_res.group(2))))
    elif line.strip():
        print(line, end="")