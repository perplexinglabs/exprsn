use std::{cell::RefCell, rc::Rc};

use crate::{ast::{ASTNode, Tree}, dfa::build_dfa, error::LangError, lexing::Token, Rrc};


pub(crate) fn parse(tokens: Vec<Token>) -> Result<Rrc<ASTNode>, LangError> {
    let mut curr_sub_tree = None;
    let mut dfa = Rc::new(RefCell::new(build_dfa()));

    for token in tokens.iter() {
        //println!("Parsing token: {:?}", token);
        let next = dfa.borrow().next(token, &curr_sub_tree);
        match next {
            Ok(Some((n, new_tree))) => {
                dfa = Rc::clone(&n);
                if let Some(t) = new_tree {
                    //println!("Setting new tree root to: {:?}", t.borrow().value);
                    curr_sub_tree = Some(Rc::clone(&t));
                }
                //curr_sub_tree.as_ref().map(|t| t.top().print_df(None));
                //println!("--------------");
            },
            Ok(None) => break,
            Err(e) => return Err(e),
        }
    }

    if let Some(t) = curr_sub_tree {
        Ok(t.top())
    } else {
        Err(LangError::ParseError("Unable to parse expression to tree".to_string()))
    }
}