use std::rc::Rc;
use std::cell::RefCell;

use crate::error::LangError;
use crate::ast::Tree;
use crate::lexing::{Token, TokenValue};
use crate::{Rrc, ASTNode};

type DFAfnResult = Result<Rrc<ASTNode>, LangError>;
type DFAfn = fn(&Option<Rrc<ASTNode>>, &Token) -> DFAfnResult;

#[derive(Debug)]
pub struct DFANode {
    pub match_token: Option<Token>,
    out: Vec<Rc<RefCell<DFANode>>>,
    on_match: Option<DFAfn>,
}

impl DFANode {
    fn new(token: Token, on_match: Option<DFAfn>) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            match_token: Some(token),
            out: vec![],
            on_match: on_match
        }))
    }

    fn new_start(nodes: Vec<Rc<RefCell<DFANode>>>) -> Self {
        Self {
            match_token: None,
            out: nodes,
            on_match: None,
        }
    }

    fn new_end() -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            match_token: None,
            out: vec![],
            on_match: None,
        }))
    }

    pub fn next(&self, token: &Token, tree: &Option<Rrc<ASTNode>>) -> Result<Option<(Rrc<Self>, Option<Rrc<ASTNode>>)>, LangError> {
        for raw_node in self.out.iter() {
            let node = raw_node.borrow();
            if raw_node.is_end() {
                return Ok(None);
            } else if token == node.match_token.as_ref().unwrap() {
                return match node.on_match {
                    Some(om) => om(tree, token).and_then(|res| Ok(Some((Rc::clone(raw_node), Some(res))))),
                    None => Ok(Some((Rc::clone(raw_node), None))),
                }
                //let new_tree = node.on_match.(|om| );

                //return Ok(Some((Rc::clone(raw_node), new_tree)));
            }
        }

        Ok(None)
    }
}

trait DFANodeTrait<T> {
    fn add_next(&self, node: &Rc<RefCell<T>>);
    fn is_end(&self) -> bool;
}

impl DFANodeTrait<DFANode> for Rc<RefCell<DFANode>> {
    fn add_next(&self, node: &Rc<RefCell<DFANode>>) {
        (**self).borrow_mut().out.push(Rc::clone(node));
    }

    fn is_end(&self) -> bool {
        let node = self.borrow();
        node.match_token.is_none() && node.out.is_empty() && node.on_match.is_none()
    }
}

fn on_compare(tree: &Option<Rrc<ASTNode>>, token: &Token) -> DFAfnResult {
    let tree = tree.as_ref().expect("Must not start comparison treeless");
    let new = ASTNode::new(token.clone());
    let mut mut_tree = tree.borrow_mut();
    if let Token::Identifier(_) = mut_tree.value {
        if let Some(ref p) = mut_tree.parent {
            p.borrow_mut().right = Some(Rc::clone(&new))
        }
        // TODO: Godbolt this new.borrow_mut() doubling up to see if intermediate var is more efficient
        new.borrow_mut().parent = mut_tree.parent.as_ref().and_then(|x| Some(Rc::clone(x)));
        new.borrow_mut().left = Some(Rc::clone(&tree));
        mut_tree.parent = Some(Rc::clone(&new));
    }

    Ok(new)
}

fn on_conjunction(tree: &Option<Rrc<ASTNode>>, token: &Token) -> DFAfnResult {
    let tree = tree.as_ref().expect("Parse error");
    let new = ASTNode::new(token.clone());

    if tree.borrow().value == Token::OpenParen {
        new.borrow_mut().left = tree.borrow().right.as_ref().map(|r| Rc::clone(r));
        new.borrow_mut().left.as_ref().map(|l| l.borrow_mut().parent = Some(Rc::clone(&new)));
        tree.borrow_mut().right = Some(Rc::clone(&new));
        new.borrow_mut().parent = Some(Rc::clone(&tree));
    } else {
        new.borrow_mut().left = Some(Rc::clone(&tree));
        new.borrow_mut().update_parent(tree);
        tree.borrow_mut().parent = Some(Rc::clone(&new));
    }

    Ok(new)
}

fn on_ident(tree: &Option<Rrc<ASTNode>>, token: &Token) -> DFAfnResult {
    let new = ASTNode::new(token.clone());
    if let Some(tree) = tree {
        tree.borrow_mut().right = Some(Rc::clone(&new));
        new.borrow_mut().parent = Some(Rc::clone(&tree));
    }

    Ok(new)
}

fn on_value(tree: &Option<Rrc<ASTNode>>, token: &Token) -> DFAfnResult {
    let new = ASTNode::new(token.clone());
    let tree = tree.as_ref().expect("Right handed values only");
    let mut mut_tree = tree.borrow_mut();
    mut_tree.right = Some(Rc::clone(&new));

    Ok(match mut_tree.parent {
        Some(ref parent) => Rc::clone(&parent),
        None => Rc::clone(&tree),
    })
}

fn on_paren(tree: &Option<Rrc<ASTNode>>, token: &Token) -> DFAfnResult {
    match token {
        Token::OpenParen => {
            let new = ASTNode::new(token.clone());
            if let Some(t) = tree {
                t.borrow_mut().right = Some(Rc::clone(&new));
                new.borrow_mut().parent = Some(Rc::clone(&t));
            }
            return Ok(new);
        },
        Token::CloseParen => {
            let tree = match tree.as_ref() {
                Some(tr) => tr,
                None => return Err(LangError::ParseError("Expression cannot start with a close paren".to_string())),
            };

            if let Some(open) = tree.find_up(|node| node.borrow().value == Token::OpenParen) {
                let open = open.borrow_mut();
                let right_subtree = open.right.as_ref().map(|o| Rc::clone(o));
                // Cut out the paren
                // open.parent.right = open.right
                open.parent_mut().map(|mut parent| parent.right = right_subtree.clone());
                // open.right.parent = open.parent
                open.right_mut().map(|mut r| r.parent = open.parent.as_ref().map(|o| Rc::clone(o)));

                let res = open.parent.as_ref()
                                .map(|p| Rc::clone(&p))
                                .or_else(|| right_subtree.as_ref().map(|o| Rc::clone(o)));

                return res.ok_or(LangError::ParseError("Failed to yeild new sub tree location from open paren".to_string()));
            } else {
                Err(LangError::ParseError("Found close paren, but missing open paren".to_string()))
            }
        },
        t => Err(LangError::ParseError(format!("on_paren DFA action function encountered an invalid token: {:?}", t)))
    }
}

pub(crate) fn build_dfa() -> DFANode {
    let eq    = DFANode::new(Token::Eq,  Some(on_compare));
    let neq   = DFANode::new(Token::Neq, Some(on_compare));
    let gt    = DFANode::new(Token::Gt,  Some(on_compare));
    let lt    = DFANode::new(Token::Lt,  Some(on_compare));
    let and   = DFANode::new(Token::And, Some(on_conjunction));
    let or    = DFANode::new(Token::Or,  Some(on_conjunction));
    let ident = DFANode::new(Token::Identifier("".to_string()), Some(on_ident));
    let value = DFANode::new(Token::Value(TokenValue::Int(0)),  Some(on_value));
    let open_paren  = DFANode::new(Token::OpenParen,  Some(on_paren));
    let close_paren = DFANode::new(Token::CloseParen, Some(on_paren));

    // Set up links between states
    and.add_next(&ident);
    and.add_next(&open_paren);
    or.add_next(&ident);
    or.add_next(&open_paren);
    open_paren.add_next(&ident);
    open_paren.add_next(&open_paren);

    for sym in [&eq, &neq, &gt, &lt] {
        sym.add_next(&ident);
        sym.add_next(&value);
        ident.add_next(sym);
    }

    ident.add_next(&and);
    ident.add_next(&or);
    ident.add_next(&close_paren);
    value.add_next(&and);
    value.add_next(&or);
    value.add_next(&close_paren);

    // Set up transition to end state
    let end = DFANode::new_end();
    ident.add_next(&end);
    close_paren.add_next(&and);
    close_paren.add_next(&or);
    close_paren.add_next(&end);

    // Set up start state
    DFANode::new_start(vec![Rc::clone(&ident),
                            Rc::clone(&open_paren),
                            Rc::clone(&end),
                           ])
}