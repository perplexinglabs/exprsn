use std::string::FromUtf8Error;

#[derive(Debug, PartialEq)]
pub enum LangError {
    Error,
    ParseError(String),
    ExpressionResultError,
    RuntimeError(String),
}

impl From<&str> for LangError {
    fn from(_: &str) -> Self {
        LangError::Error
    }
}

impl From<FromUtf8Error> for LangError {
    fn from(_: FromUtf8Error) -> Self {
        LangError::Error
    }
}