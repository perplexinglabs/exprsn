use std::{cell::{Ref, RefCell, RefMut}, rc::Rc};

use crate::{lexing::Token, Rrc};

#[derive(Debug)]
pub(crate) struct ASTNode {
    pub(crate) left:   Option<Rrc<ASTNode>>,
    pub(crate) right:  Option<Rrc<ASTNode>>,
    pub(crate) parent: Option<Rrc<ASTNode>>,
    pub(crate) value: Token,
}

impl ASTNode {
    pub(crate) fn new(value: Token) -> Rrc<Self> {
        Rc::new(RefCell::new(Self {
            left: None,
            right: None,
            parent: None,
            value,
        }))
    }

    pub(crate) fn left_ref(&self) -> Option<Ref<ASTNode>> {
        self.left.as_ref().and_then(|l| Some(l.borrow()))
    }
    pub(crate) fn right_ref(&self) -> Option<Ref<ASTNode>> {
        self.right.as_ref().and_then(|r| Some(r.borrow()))
    }

    pub(crate) fn right_mut(&self) -> Option<RefMut<ASTNode>> {
        self.right.as_ref().and_then(|r| Some(r.borrow_mut()))
    }
    pub(crate) fn parent_mut(&self) -> Option<RefMut<ASTNode>> {
        self.parent.as_ref().and_then(|r| Some(r.borrow_mut()))
    }

    pub(crate) fn update_parent(&mut self, other_node: &Rrc<ASTNode>) {
        self.parent = other_node.borrow().parent.as_ref().and_then(|x| Some(Rc::clone(x)));

    }
}


pub(crate) trait Tree<T> {
    fn top(&self) -> T;
    fn find_up(&self, predicate: fn(&T) -> bool) -> Option<T>;
    fn print_df(&self, depth: Option<u32>);
}

impl Tree<Rrc<ASTNode>> for Rrc<ASTNode> {
    fn top(&self) -> Rrc<ASTNode> {
        match self.borrow().parent {
            Some(ref p) => p.top(),
            None => Rc::clone(self)
        }
    }

    fn print_df(&self, depth: Option<u32>) {
        let d = depth.unwrap_or_default();

        let nv = self.borrow();
        let dash_indent = "-- ".repeat(d.try_into().unwrap());
        let spac_indent = "   ".repeat(d.try_into().unwrap());
        println!("{}node value: {:?}", dash_indent, nv.value);
        println!("{}parent    : {:?}", spac_indent, nv.parent.as_ref().map(|l| l.borrow().value.clone()));
        println!("{}left      : {:?}", spac_indent, nv.left.as_ref().map(|l| l.borrow().value.clone()));
        println!("{}right     : {:?}", spac_indent, nv.right.as_ref().map(|l| l.borrow().value.clone()));
        if let Some(ref l) = nv.left {
            l.print_df(Some(d + 1));
        }
        if let Some(ref r) = nv.right {
            r.print_df(Some(d + 1));
        }
    }

    fn find_up(&self, predicate: fn(&Rrc<ASTNode>) -> bool) -> Option<Rrc<ASTNode>> {
        // Test self
        if predicate(self) {
            return Some(Rc::clone(self));
        }

        // Then search recursively upwards
        match self.borrow().parent {
            Some(ref p) if predicate(p) => Some(Rc::clone(p)),
            Some(ref p) => p.find_up(predicate),
            None => None,
        }
    }
}