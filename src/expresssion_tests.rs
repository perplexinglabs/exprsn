use std::collections::HashMap;
use maplit::hashmap;
use crate::{error::LangError, eval, TokenValue};


#[test]
fn test_empty_string() {
    let example = "a != ''";

    let names: HashMap<&str, TokenValue> = hashmap!{
        "a" => TokenValue::Str("".to_string()),
    };

    let res = eval(example, &names).unwrap();

    assert_eq!(res, false);
}

#[test]
fn test_many_cond() {
    let example = "a == true and (b == 'voronoi' or b == 'voronoigrid' or b == 'voronoihexgrid')";

    let names: HashMap<&str, TokenValue> = hashmap!{
        "a" => false.into(),
        "b" => TokenValue::Str("voronoi".to_string()),
    };

    let res = eval(example, &names).unwrap();

    assert_eq!(res, false);
}

#[test]
fn test_parens() {
    let example = "a == true and b != true and (c > 1 or d > 1)";

    let names: HashMap<&str, TokenValue> = hashmap!{
        "a" => false.into(),
        "b" => false.into(),
        "c" => 1.into(),
        "d" => 2.into(),
    };

    let res = eval(example, &names).unwrap();

    assert_eq!(res, false);
}

#[test]
fn test_paren_start() {
    let example = "(a == true)";

    let names: HashMap<&str, TokenValue> = hashmap!{
        "a" => true.into(),
    };

    let res = eval(example, &names).unwrap();

    assert_eq!(res, true);
}

#[test]
fn test_and() {
    let example = "a == true and b == true";

    let mut names: HashMap<&str, TokenValue> = hashmap!{
        "a" => true.into(),
        "b" => true.into(),
    };

    let r1 = eval(example, &names).unwrap();
    assert_eq!(r1, true);

    names.insert("a", false.into());
    let r2 = eval(example, &names).unwrap();
    assert_eq!(r2, false);

    names.insert("b", false.into());
    let r3 = eval(example, &names).unwrap();
    assert_eq!(r3, false);

    names.insert("a", true.into());
    let r4 = eval(example, &names).unwrap();
    assert_eq!(r4, false);
}

#[test]
fn test_and_bare() {
    let example = "a and b";

    let mut names: HashMap<&str, TokenValue> = hashmap!{
        "a" => true.into(),
        "b" => true.into(),
    };

    let r1 = eval(example, &names).unwrap();
    assert_eq!(r1, true);

    names.insert("a", false.into());
    let r2 = eval(example, &names).unwrap();
    assert_eq!(r2, false);

    names.insert("b", false.into());
    let r3 = eval(example, &names).unwrap();
    assert_eq!(r3, false);

    names.insert("a", true.into());
    let r4 = eval(example, &names).unwrap();
    assert_eq!(r4, false);
}

#[test]
fn test_or() {
    let example = "a == true or b == true";

    let mut names: HashMap<&str, TokenValue> = hashmap!{
        "a" => true.into(),
        "b" => true.into(),
    };

    let r1 = eval(example, &names).unwrap();
    assert_eq!(r1, true);

    names.insert("a", false.into());
    let r2 = eval(example, &names).unwrap();
    assert_eq!(r2, true);

    names.insert("b", false.into());
    let r3 = eval(example, &names).unwrap();
    assert_eq!(r3, false);

    names.insert("a", true.into());
    let r4 = eval(example, &names).unwrap();
    assert_eq!(r4, true);
}

#[test]
fn test_triple_or() {
    let example = "a == true or b == true or c == true";

    let names: HashMap<&str, TokenValue> = hashmap!{
        "a" => false.into(),
        "b" => false.into(),
        "c" => true.into(),
    };

    let r1 = eval(example, &names).unwrap();
    assert_eq!(r1, true);
}

#[test]
fn test_or_bare() {
    let example = "a or b";

    let mut names: HashMap<&str, TokenValue> = hashmap!{
        "a" => true.into(),
        "b" => true.into(),
    };

    let r1 = eval(example, &names).unwrap();
    assert_eq!(r1, true);

    names.insert("a", false.into());
    let r2 = eval(example, &names).unwrap();
    assert_eq!(r2, true);

    names.insert("b", false.into());
    let r3 = eval(example, &names).unwrap();
    assert_eq!(r3, false);

    names.insert("a", true.into());
    let r4 = eval(example, &names).unwrap();
    assert_eq!(r4, true);
}

#[test]
fn test_bare() {
    let example = "a";

    let mut names: HashMap<&str, TokenValue> = hashmap!{
        "a" => true.into(),
    };

    let r1 = eval(example, &names).unwrap();
    assert_eq!(r1, true);

    names.insert("a", false.into());
    let r2 = eval(example, &names).unwrap();
    assert_eq!(r2, false);
}

#[test]
fn test_open_with_paren() {
    let example = ") a == 1";

    let names: HashMap<&str, TokenValue> = hashmap!{
        "a" => 1.into(),
    };

    let r1 = eval(example, &names);
    assert!(matches!(r1, Err(LangError::ParseError(_))));

}

#[test]
fn test_missing_open() {
    let example = "a == 1)";

    let names: HashMap<&str, TokenValue> = hashmap!{
        "a" => 1.into(),
    };

    let r1 = eval(example, &names);
    assert!(matches!(r1, Err(LangError::ParseError(_))));

}