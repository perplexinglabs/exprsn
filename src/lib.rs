pub mod error;
mod lexing;
mod parsing;
mod dfa;
mod ast;

#[cfg(test)]
mod expresssion_tests;

use std::{cell::RefCell, collections::HashMap, rc::Rc, hash::Hash};
use std::borrow;

use ast::{ASTNode, Tree};
use error::LangError;
use lexing::{lex, Token};
use parsing::parse;

// Import name publicly at this level
pub use lexing::TokenValue;


type Rrc<T> = Rc<RefCell<T>>;

/// Represents a "compiled" expression which can then be quickly evaluated via
/// `execute`
pub struct Expression {
    compiled: Rrc<ASTNode>,
}

impl Expression {
    pub fn print_df(&self) {
        self.compiled.print_df(None);
    }
}

/// Compiles a string to an `Expression`
///
/// # Parameters
///
/// * `expression` - A string representing a valid expression
pub fn compile(expression: &str) -> Result<Expression, LangError> {
    let tokens = lex(expression)?;
    Ok(Expression {
        compiled: parse(tokens)?,
    })
}

/// Evaluates a compiled expression using the specified variables
///
/// # Parameters
///
/// * `expression` - A compiled expression struct
/// * `variables` - Hashmap mapping the current state of all variables used in the expression
pub fn execute<K, V>(expression: Expression, variables: &HashMap<K, V>) -> Result<bool, LangError>
    where K: borrow::Borrow<str> + Hash + Eq,
          V: Into<TokenValue> + Clone
{
    let borrow = expression.compiled.borrow();
    evaluate(&borrow, variables).and_then(|e| match e {
        TokenValue::Bool(b) => Ok(b),
        _ => Err(LangError::ExpressionResultError)
    })
}

/// Evaluate a string as an expresion using the specified variables
///
/// # Parameters
///
/// * `expression` - A string representing a valid expression
/// * `variables` - Hashmap mapping the current state of all variables used in the expression
pub fn eval<K, V>(expression: &str, variables: &HashMap<K, V>) -> Result<bool, LangError>
    where K: borrow::Borrow<str> + Hash + Eq,
          V: Into<TokenValue> + Clone
{
    let tokens = lex(expression)?;
    let tree = parse(tokens)?;

    let borrow = tree.borrow();
    evaluate(&borrow, variables).and_then(|e| match e {
        TokenValue::Bool(b) => Ok(b),
        _ => Err(LangError::ExpressionResultError)
    })
}

fn evaluate<K, V>(node: &ASTNode, names: &HashMap<K, V>) -> Result<TokenValue, LangError>
    where K: borrow::Borrow<str> + Hash + Eq,
          V: Into<TokenValue> + Clone
{
    let left  = node.left_ref();
    let right = node.right_ref();

    match node.value {
        Token::Value(ref v) => Ok(v.clone()),
        Token::Identifier(ref i) => {
            let token_value = names.get(&i).expect(&format!("No value for identifier: {:?}", i)).clone();
            Ok(token_value.into())
        },
        Token::Eq => {
            let lv = evaluate(&left.unwrap(),  names)?;
            let rv = evaluate(&right.unwrap(), names)?;
            Ok(TokenValue::Bool(lv == rv))
        },
        Token::Neq => {
            let lv = evaluate(&left.unwrap(),  names)?;
            let rv = evaluate(&right.unwrap(), names)?;
            Ok(TokenValue::Bool(lv != rv))
        },
        Token::Gt => {
            let lv = evaluate(&left.unwrap(),  names)?;
            let rv = evaluate(&right.unwrap(), names)?;
            match lv.try_gt(&rv) {
                Ok(v) => Ok(TokenValue::Bool(v)),
                Err(()) => Err(LangError::RuntimeError(format!("Cannot compare `{:?}` with `{:?}`", lv, rv).to_string())),
            }
        },
        Token::Lt => {
            let lv = evaluate(&left.unwrap(),  names)?;
            let rv = evaluate(&right.unwrap(), names)?;
            match rv.try_gt(&lv) {
                Ok(v) => Ok(TokenValue::Bool(v)),
                Err(()) => Err(LangError::RuntimeError(format!("Cannot compare `{:?}` with `{:?}`", lv, rv).to_string())),
            }
        },
        Token::And => {
            let lv = evaluate(&left.unwrap(),  names)?;
            let rv = evaluate(&right.unwrap(), names)?;
            if let (TokenValue::Bool(l), TokenValue::Bool(r)) = (lv, rv) {
                Ok(TokenValue::Bool(l && r))
            } else {
                panic!("This should never happen");
            }
        },
        Token::Or => {
            let lv = evaluate(&left.unwrap(),  names)?;
            let rv = evaluate(&right.unwrap(), names)?;
            if let (TokenValue::Bool(l), TokenValue::Bool(r)) = (lv, rv) {
                Ok(TokenValue::Bool(l || r))
            } else {
                panic!("This should never happen");
            }
        },
        Token::OpenParen  => panic!("Hit an OpenParen token which should never happen"),
        Token::CloseParen => panic!("Hit a CloseParen token which should never happen"),
    }
}