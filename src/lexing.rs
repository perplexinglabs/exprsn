use std::fmt::Display;

use crate::error::LangError;

#[derive(Debug, Clone)]
pub(crate) enum Token {
    Value(TokenValue),
    Identifier(String),
    Eq,
    Neq,
    Gt,
    Lt,
    OpenParen,
    CloseParen,
    And,
    Or,
}

impl PartialEq for Token {
    fn eq(&self, other: &Self) -> bool {
        core::mem::discriminant(self) == core::mem::discriminant(other)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum TokenValue {
    Bool(bool),
    Int(i32),
    Float(f32),
    Str(String),
}

impl Display for TokenValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let args = match self {
            TokenValue::Bool(b)  => format!("{}", b),
            TokenValue::Int(i)   => format!("{}", i),
            TokenValue::Float(f) => format!("{}", f),
            TokenValue::Str(s)   => format!("{}", s),
        };
        f.write_str(&args)
    }
}

impl TokenValue {
    /// Attempt to compare two TokenValue instances.
    /// May fail if the data types are not comparable.
    /// Allowed comparable types are: Int, Float (in any order)
    pub fn try_gt(&self, other: &Self) -> Result<bool, ()> {
        use TokenValue::*;

        match (self, other) {
            (Int(s), Int(o))     => Ok(s > o),
            (Float(s), Float(o)) => Ok(s > o),
            (Int(s), Float(o))   => Ok((*s as f32) > *o),
            (Float(s), Int(o))   => Ok(*s > (*o as f32)),
            (_, _) => Err(()),
        }
    }
}

impl From<bool> for TokenValue {
    fn from(value: bool) -> Self {
        Self::Bool(value)
    }
}

impl From<i32> for TokenValue {
    fn from(value: i32) -> Self {
        Self::Int(value)
    }
}

impl From<f32> for TokenValue {
    fn from(value: f32) -> Self {
        Self::Float(value)
    }
}

impl TryFrom<&str> for TokenValue {
    type Error = LangError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        use TokenValue::*;

        if value.contains('.') {
            if let Ok(s) = value.parse() {
                return Ok(Float(s));
            }
        }
        if let Ok(s) = value.parse() {
            return Ok(Int(s));
        }
        if let Ok(s) = value.parse() {
            return Ok(Bool(s));
        }

        Ok(Str(value.to_string()))
    }
}

pub fn lex(input: &str) -> Result<Vec<Token>, LangError> {
    use Token::*;
    use TokenValue::*;

    // buffer contains multiple characters which constitute one of:
    // 1. String 2. Identifier 3. Keyword (e.g. "and", "or", "true", "false", etc.)
    let mut buffer = vec![];
    let mut tokens = vec![];

    let mut eq_start = false;
    let mut neq_start = false;
    let mut quote_open = false;
    let mut add_as_string = false;
    let mut escaping = false;

    for c in input.as_bytes() {
        let mut matched_buffer = false;
        let pre_next_len = tokens.len();

        if quote_open { match c {
            b'\'' => {
                if escaping {
                    matched_buffer = true;
                    buffer.push(*c);
                    escaping = false;
                } else {
                    quote_open = false;
                    add_as_string = true;
                }
            },
            b'\\' => {
                if escaping {
                    matched_buffer = true;
                    buffer.push(*c);
                    escaping = false;
                } else {
                    escaping = true;
                }
            },
            // Matches any character in a string
            _ => {
                matched_buffer = true;
                buffer.push(*c);
            }
        }} else { match c {
            b'\'' => quote_open = true,
            b'(' => tokens.push(OpenParen),
            b')' => tokens.push(CloseParen),
            b'=' => {
                if !eq_start && !neq_start {
                    eq_start = true;
                } else if eq_start {
                    tokens.push(Eq);
                    eq_start = false;
                } else if neq_start {
                    tokens.push(Neq);
                    neq_start = false;
                }
            },
            b'!' => neq_start = true,
            b'<' => tokens.push(Lt),
            b'>' => tokens.push(Gt),
            // Matches limited range of ascii for identifiers and keywords
            46 | 48..=57 | 65..=90 | 95 | 97..=122 => {
                matched_buffer = true;
                buffer.push(*c);
            },
            _ => {},
        }};
        let new_in_between = tokens.len() > pre_next_len;

        if add_as_string {
            let buff_str = String::from_utf8(buffer.clone()).unwrap();
            let parsed = TokenValue::try_from(buff_str.as_str())?;

            tokens.push(Value(parsed));
            add_as_string = false;

            buffer.clear();
        } else if !matched_buffer && !buffer.is_empty() && !escaping {
            let buff_str = String::from_utf8(buffer.clone()).unwrap();
            let parsed = TokenValue::try_from(buff_str.as_str());

            if buff_str == "and" {
                tokens.push(And);
            } else if buff_str == "or" {
                tokens.push(Or);
            } else if let Ok(Bool(token_value)) = parsed {
                tokens.push(Value(Bool(token_value)));
            } else if let Ok(Int(token_value)) = parsed {
                tokens.push(Value(Int(token_value)));
            } else if let Ok(Float(token_value)) = parsed {
                tokens.push(Value(Float(token_value)));
            } else {
                tokens.push(Identifier(buff_str));
            }

            buffer.clear();

            if new_in_between {
                let last_i = tokens.len();
                tokens.swap(last_i - 2, last_i - 1);
            }
        }
    }

    if !buffer.is_empty() {
        let buff_str = String::from_utf8(buffer.clone()).unwrap();
        let parsed = TokenValue::try_from(buff_str.as_str());
        if buff_str == "and" {
            tokens.push(And);
        } else if buff_str == "or" {
            tokens.push(Or);
        } else if add_as_string {
            tokens.push(Value(parsed?));
        } else if let Ok(Bool(token_value)) = parsed {
            tokens.push(Value(Bool(token_value)));
        } else if let Ok(Int(token_value)) = parsed {
            tokens.push(Value(Int(token_value)));
        } else if let Ok(Float(token_value)) = parsed {
            tokens.push(Value(Float(token_value)));
        } else {
            tokens.push(Identifier(buff_str));
        }
        buffer.clear();
    }

    Ok(tokens)
}